package com.distributed.hw4;

import java.io.File;
import java.util.ArrayList;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext; 

import scala.Tuple2;

public class AssignmentParta 
{
		
	private static String[] calcPercenta(String[] row, String fileName){
	    ArrayList<String> outRow = new ArrayList<String>();
	    outRow.add(fileName);
	    Float percent = (Float.parseFloat(row[4]) - Float.parseFloat(row[1]))/Float.parseFloat(row[1])*100;
	    outRow.add(percent.toString());
	    return outRow.toArray(new String[outRow.size()]);
	  }	
			
	public static void main(String[] args){
		
		String dataPath = args[0];
		String outPath = args[1];
		
		File folder = new File(dataPath);
		File[] listOfFiles = folder.listFiles();
		
		SparkConf conf =  new SparkConf().setMaster("local").setAppName("App");
		JavaSparkContext sc = new JavaSparkContext(conf);
		
		JavaRDD <String []> outRdd = sc.emptyRDD();
		
		for(int i=0; i<listOfFiles.length; i++){
			String file = listOfFiles[i].getAbsolutePath();
			JavaRDD <String> data = sc.textFile(file);
			String fileName = listOfFiles[i].getName().split(".csv")[0];
			String header = data.first();
			JavaRDD<String[]> dataArray = data.filter(row->(!row.equals(header))).
					map(row-> row.split(",")).
					map(row->calcPercenta(row, fileName)).filter(row->(Float.parseFloat(row[1])>=1.0));
			outRdd = outRdd.union(dataArray);
		}
		JavaPairRDD<String, Integer> outRddPair = outRdd.mapToPair(row->(new Tuple2<String, Integer>(row[0], 1))).reduceByKey((x,y)->(x+y)).sortByKey();
		outRddPair = outRddPair.repartition(1);
		JavaRDD<String> finalOut = outRddPair.map(row->row._1 + ","+row._2);
		finalOut.saveAsTextFile(outPath);
		sc.stop();
		sc.close();
	}
	
}
