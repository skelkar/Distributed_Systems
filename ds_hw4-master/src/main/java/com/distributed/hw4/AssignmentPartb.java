package com.distributed.hw4;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext; 

import scala.Tuple2;


class CompProfObj{
	
	private String Comp;
	private Float Prof;
	
	public String getComp() {
		return Comp;
	}
	public void setComp(String comp) {
		Comp = comp;
	}
	public Float getProf() {
		return Prof;
	}
	public void setProf(Float prof) {
		Prof = prof;
	}
}

public class AssignmentPartb 
{
	private static String[] calcPercentb(String[] row, String fileName){
	    ArrayList<String> outRow = new ArrayList<String>();
	    outRow.add(row[0]);
	    Float percent = (Float.parseFloat(row[4]) - Float.parseFloat(row[1]))/Float.parseFloat(row[1])*100;
	    outRow.add(fileName + "_" + percent.toString());
	    return outRow.toArray(new String[outRow.size()]);
	  }	
	
	private static Tuple2<String, ArrayList<String>> mapFunc(String date, ArrayList<String> old, String newElement){
		old.add(newElement);
		return new Tuple2<String, ArrayList<String>>(date, old);
	  }	
	
	private static Tuple2<String, ArrayList<String>> sortFunc(String date, ArrayList<String> compProfIn){
		ArrayList<CompProfObj> TopFiveProf = new ArrayList<>();
		for(int i=0; i<compProfIn.size(); i++){
			String[] compProfArr = compProfIn.get(i).split("_");
			CompProfObj compProfObj = new CompProfObj();
			compProfObj.setComp(compProfArr[0]);
			compProfObj.setProf(Float.parseFloat(compProfArr[1]));
			TopFiveProf.add(compProfObj);
		}
		Collections.sort(TopFiveProf, (o1, o2) -> o1.getProf().compareTo(o2.getProf()));
		Collections.reverse(TopFiveProf);
		
		ArrayList<String> comp = (ArrayList<String>) TopFiveProf.stream().limit(5).map(CompProfObj::getComp).collect(Collectors.toList());

		return new Tuple2<String, ArrayList<String>>(date, comp);
	  }	
	
	public static void main(String[] args){
		
		String dataPath = args[0];
		String outPath = args[1];
		
		File folder = new File(dataPath);
		File[] listOfFiles = folder.listFiles();
		
		SparkConf conf =  new SparkConf().setMaster("local").setAppName("App");
		JavaSparkContext sc = new JavaSparkContext(conf);

		JavaPairRDD<String, ArrayList<String>> outRdd = null;
		
		for(int i=0; i<listOfFiles.length; i++){
			String file = listOfFiles[i].getAbsolutePath();
			JavaRDD <String> data = sc.textFile(file);
			String fileName = listOfFiles[i].getName().split(".csv")[0];
			if(fileName.contains("json")){
				continue;
			}
			String header = data.first();
			JavaRDD<String[]> dataArray = data.filter(row->(!row.equals(header))).
					map(row-> row.split(",")).
					map(row->calcPercentb(row, fileName));	
			if(i==0){
				outRdd = dataArray
						.mapToPair(row-> new Tuple2<String, ArrayList<String>>(row[0], 
								new ArrayList<String>(Arrays.asList(row[1]))));
			}
			else{
				JavaPairRDD<String, String> dataArrayPair = 
						dataArray.mapToPair(row -> (new Tuple2<String, String>(row[0], row[1])));
				outRdd = outRdd.join(dataArrayPair)
				.mapToPair(row -> mapFunc(row._1, row._2._1, row._2._2));
			}
		}
		outRdd = outRdd.mapToPair(row -> sortFunc(row._1, row._2)).sortByKey();
		outRdd = outRdd.repartition(1);
		JavaRDD<String> finalOut = outRdd.map(row->row._1 + ","+row._2).map(row->row.replace(" ", ""));
		finalOut.saveAsTextFile(outPath);
	
		sc.stop();
		sc.close();
	}
	
}
