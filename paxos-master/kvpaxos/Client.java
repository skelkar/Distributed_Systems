package kvpaxos;

//import sun.misc.Request;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class Client {
    String[] servers;
    int[] ports;
    // Your data here
    boolean debug = true;

    public Client(String[] servers, int[] ports){
        this.servers = servers;
        this.ports = ports;
        // Your initialization code here
    }

    /**
     * Call() sends an RMI to the RMI handler on server with
     * arguments rmi name, request message, and server id. It
     * waits for the reply and return a response message if
     * the server responded, and return null if Call() was not
     * be able to contact the server.
     *
     * You should assume that Call() will time out and return
     * null after a while if it doesn't get a reply from the server.
     *
     * Please use Call() to send all RMIs and please don't change
     * this function.
     */
    public Response Call(String rmi, Request req, int id){
        Response callReply = null;
        KVPaxosRMI stub;
        try{
            Registry registry= LocateRegistry.getRegistry(this.ports[id]);
            stub=(KVPaxosRMI) registry.lookup("KVPaxos");
            if(rmi.equals("Get"))
                callReply = stub.Get(req);
            else if(rmi.equals("Put")){
                callReply = stub.Put(req);}
            else
                System.out.println("Wrong parameters!");
        } catch(Exception e){
            return null;
        }
        return callReply;
    }

    // RMI handlers
    public Integer Get(String key){
        // Your code here
        int clientSeq = 0;
        if(debug){ System.out.println("In Put and key + " + key );}
        Op getOp = new Op("Get",clientSeq,key,0);
        Response reply = null;
        Request req = new Request();
        req.op = getOp;
        int i = 0;

        while(true){
            if(debug){System.out.println("This is clientSeq in get call: " + clientSeq);}
            reply = Call("Get",req,i);
            if(reply == null) {
                if(debug){System.out.println("Get returned null");}
                i = (i + 1) % servers.length;
            }
            else if(reply.Reply == 1){
                if(debug){System.out.println("Get succeeded");}
                //Get succeeded
                break;
            }
            else if(reply.Reply == 0){
                //Get failed, try new sequence
                clientSeq = reply.op.ClientSeq;
                getOp.ClientSeq = reply.op.ClientSeq;
                if(debug){System.out.println("Get Failed this is new clientSeq: " + clientSeq);}
            }
            req.op = getOp;
        }


        return reply.op.value;
    }

    public boolean Put(String key, Integer value){
        // Your code here
        int clientSeq = 0;
        if(debug){ System.out.println("In Put and key + " + key + " and value " + value);}
        Op putOp = new Op("Put",clientSeq,key,value);
        Response reply = null;
        Request req = new Request();
        req.op = putOp;
        int i = 0;
        while(true){
            if(debug){System.out.println("This is clientSeq in put call: " + clientSeq);}
            reply = Call("Put",req,i);
            if(reply == null) {
                if(debug){System.out.println("Put returned null");}
                i = (i + 1) % servers.length;
            }
            else if(reply.Reply == 1){
                //Put succeeded
                if(debug){System.out.println("Put succeeded");}
                break;
            }
            else if(reply.Reply == 0){
                //Put failed, try new sequence
                clientSeq = reply.op.ClientSeq;
                putOp.ClientSeq = reply.op.ClientSeq;
                if(debug){System.out.println("Put Failed this is new clientSeq: " + clientSeq);}
            }
            req.op = putOp;
        }

        return true;

    }

}
