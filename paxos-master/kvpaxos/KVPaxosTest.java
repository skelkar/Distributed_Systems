package kvpaxos;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This is a subset of entire test cases
 * For your reference only.
 */
public class KVPaxosTest {


    public void check(Client ck, String key, Integer value){
        Integer v = ck.Get(key);
        assertTrue("Get(" + key + ")->" + v + ", expected " + value, v.equals(value));
    }

    @Test
    public void TestBasic(){
        final int npaxos = 5;
        String host = "127.0.0.1";
        String[] peers = new String[npaxos];
        int[] ports = new int[npaxos];

        Server[] kva = new Server[npaxos];
        for(int i = 0 ; i < npaxos; i++){
            ports[i] = 1100+i;
            peers[i] = host;
        }
        for(int i = 0; i < npaxos; i++){
            kva[i] = new Server(peers, ports, i);
        }


        Client ck0 = new Client(peers, ports);
        Client ck1 = new Client(peers, ports);
        Client ck2 = new Client(peers, ports);

        // Put and get to same client
        ck0.Put("a", 1);
        check(ck0, "a", 1);

        // Put different value at another client and check if it reflects everywhere
        ck1.Put("a", 2);
        check(ck0, "a", 2);
        check(ck1, "a", 2);
        check(ck2, "a", 2);

//        System.out.println("Test: Basic put/get ...");
//        ck0.Put("app", 6);
//        kva[0].ports[2] = 1;
//        ck1.Put("app", 7);
//        ck2.ports[0] = 1;
//        ck2.ports[1] = 1;
//        check(ck2, "app", 7);
        /*
        ck.Put("a", 70);
        check(ck, "a", 70);
        */
        System.out.println("... Passed");

    }

}
