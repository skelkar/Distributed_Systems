package kvpaxos;
import java.io.Serializable;

/**
 * You may find this class useful, free to use it.
 */
public class Op implements Serializable{
    static final long serialVersionUID=33L;
    String op;
    int ClientSeq;
    String key;
    Integer value;

    public Op(String op, int ClientSeq, String key, Integer value){
        this.op = op;
        this.ClientSeq = ClientSeq;
        this.key = key;
        this.value = value;
    }

    public boolean equals(Op them){
        System.out.println("Hey we are comparing Op Objects");
        if(them.op.equals(op) && them.ClientSeq == ClientSeq
                && them.key.equals(key) && value.equals(them.value)){
            return true;
        }
        return false;
    }
}
