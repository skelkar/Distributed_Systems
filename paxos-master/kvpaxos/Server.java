package kvpaxos;
import paxos.Paxos;
import paxos.State;
// You are allowed to call Paxos.Status to check if agreement was made.

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.locks.ReentrantLock;

public class Server implements KVPaxosRMI {

    ReentrantLock mutex;
    Registry registry;
    Paxos px;
    int me;

    String[] servers;
    int[] ports;
    KVPaxosRMI stub;

    // Your definitions here
    int seqGold;
    boolean debug = true;

    public Server(String[] servers, int[] ports, int me){
        this.me = me;
        this.servers = servers;
        this.ports = ports;
        this.mutex = new ReentrantLock();
        this.px = new Paxos(me, servers, ports);
        // Your initialization code here
        int seqGold = 0;


        try{
            System.setProperty("java.rmi.server.hostname", this.servers[this.me]);
            registry = LocateRegistry.getRegistry(this.ports[this.me]);
            stub = (KVPaxosRMI) UnicastRemoteObject.exportObject(this, this.ports[this.me]);
            registry.rebind("KVPaxos", stub);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public Op wait (int seq) {
        int to = 10;
        while (true) {
            Paxos.retStatus ret = this.px.Status(seq);
            if (ret.state == State.Decided) {
                return Op.class.cast(ret.v);
            }
            try {
                Thread.sleep(to);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (to < 1000) {
                to = to * 2;
            }
        }
    }

    public Integer getValue(String key){
        int seqMax = this.px.Max();
        int seqMin = this.px.Min();
        if(debug){System.out.println("This is seqMax: " + seqMax + " seqMin: " + seqMin);}
        for(int i = seqMax; i > seqMin - 1; i--){
            if(this.px.seqInstanceMap.containsKey(i)){
                Op op = (Op) this.px.seqInstanceMap.get(i).decidedValue;
                if(op.key.equals(key)){
                    return op.value;
                }
            }
        }
        return null;

    }

    public void updateSeqGold(){
        for(int i = seqGold; i < this.px.Max(); i++){
            if(this.px.seqInstanceMap.containsKey(i) && this.px.seqInstanceMap.get(i).decidedValue != null){
                seqGold++;
            }
            else{
                if(debug){System.out.println("Returning with seqGold at this: " + seqGold);}
                break;
            }
        }
    }

    // RMI handlers
    public Response Get(Request req){
        // Your code here
        updateSeqGold();
        int seqMin = this.px.Min();
        int proposedSeq = req.op.ClientSeq;
        Response resp = new Response();
        req.op.value = getValue(req.op.key);
        resp.op = new Op(req.op.op, req.op.ClientSeq, req.op.key, req.op.value);
        if(req.op.value == null){
            System.out.println("Not sure what to do here yet");
            return resp;
        }
        if(debug){System.out.println("This is value gotten " + req.op.value);}
        if(proposedSeq < seqMin){
            resp.Reply = 0;
            resp.op.ClientSeq = seqGold + 1;
            return resp;
        }
        if(debug){System.out.println("Get:<1>");}
        if(this.px.seqInstanceMap.get(proposedSeq) == null){
            //you are not in the log
            if(debug){System.out.println("Get:<2>");}
            if(proposedSeq > this.px.Max()){
                //you are bigger than the log max so we will start you
                if(debug){System.out.println("Get:<3>");}
                this.px.Start(proposedSeq,req.op);
                Op decidedOp = wait(proposedSeq);
                if(decidedOp.equals(req.op)){
                    //we started you, and you won so youre good
                    if(debug){System.out.println("Get:<4>");}
                    resp.Reply = 1;
                    return resp;
                }
                else{
                    //we started you and someone else won, get rekt brah, try again
                    if(debug){System.out.println("Get:<5>");}
                    resp.Reply = 0;
                    resp.op.ClientSeq = seqGold + 1;
                    return resp;
                }

            }
            else{
                //you are not in the log but not the max, so wtf bro
                System.out.println("Should never be here get 1");
            }
        }
        else if(this.px.seqInstanceMap.get(proposedSeq).decidedValue != null){
            if(debug){System.out.println("Get:<6>");}
            if(this.px.seqInstanceMap.get(proposedSeq).decidedValue.equals(req.op)){
                //your in the log and equal, so youre good
                if(debug){System.out.println("Get:<7>");}
                resp.Reply = 1;
                return resp;
            }
            else{
                //your in the log but not equal, so need a new sequence
                if(debug){System.out.println("Get:<8>");}
                resp.Reply = 0;
                resp.op.ClientSeq = seqGold + 1;
                return resp;
            }
        }
        else{
            //you are in the log, but not decided, so need to wait
            Op decidedOp = wait(proposedSeq);
            if(decidedOp.equals(req.op)){
                //you got decided to be in the log, so youre good
                if(debug){System.out.println("Get:<9>");}
                resp.Reply = 1;
                return resp;
            }
            else{
                //someone beat you out during the wait, so need a new seq
                if(debug){System.out.println("Get:<10>");}
                resp.Reply = 0;
                resp.op.ClientSeq = seqGold + 1;
                return resp;
            }
        }

        System.out.println("Should not be here get 3");
        return new Response();
    }

    public Response Put(Request req){
        // Your code here
        updateSeqGold();
        int seqMin = this.px.Min();
        int proposedSeq = req.op.ClientSeq;
        Response resp = new Response();
        resp.op = new Op(req.op.op, req.op.ClientSeq, req.op.key, req.op.value);
        if(proposedSeq < seqMin){
            resp.Reply = 0;
            resp.op.ClientSeq = seqGold + 1;
            return resp;
        }
        if(this.px.seqInstanceMap.get(proposedSeq) == null){
            //you are not in the log
            if(proposedSeq > this.px.Max()){
                //you are bigger than the log max so we will start you
                this.px.Start(proposedSeq,req.op);
                Op decidedOp = wait(proposedSeq);
                if(decidedOp.equals(req.op)){
                    //we started you, and you won so youre good
                    resp.Reply = 1;
                    return resp;
                }
                else{
                    //we started you and someone else won, get rekt brah, try again
                    resp.Reply = 0;
                    resp.op.ClientSeq = seqGold + 1;
                    return resp;
                }

            }
            else{
                //you are not in the log but not the max, so wtf bro
                System.out.println("Should never be here 1");
            }
        }
        else if(this.px.seqInstanceMap.get(proposedSeq).decidedValue != null){
            if(this.px.seqInstanceMap.get(proposedSeq).decidedValue.equals(req.op)){
                //your in the log and equal, so youre good
                resp.Reply = 1;
                return resp;
            }
            else{
                //your in the log but not equal, so need a new sequence
                resp.Reply = 0;
                resp.op.ClientSeq = seqGold + 1;
                return resp;
            }
        }
        else{
            //you are in the log, but not decided, so need to wait
            Op decidedOp = wait(proposedSeq);
            if(decidedOp.equals(req.op)){
                //you got decided to be in the log, so youre good
                resp.Reply = 1;
                return resp;
            }
            else{
                //someone beat you out during the wait, so need a new seq
                resp.Reply = 0;
                resp.op.ClientSeq = seqGold + 1;
                return resp;
            }
        }

        System.out.println("Should not be here 3");
        return new Response();
    }


}
