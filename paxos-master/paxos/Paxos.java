package paxos;

import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;
import java.util.Random;

/**
 * This class is the main class you need to implement paxos instances.
 */
public class Paxos implements PaxosRMI, Runnable{

    ReentrantLock mutex;
    String[] peers; // hostname
    int[] ports; // host port
    int me; // index into peers[]

    Registry registry;
    PaxosRMI stub;

    AtomicBoolean dead;// for testing
    AtomicBoolean unreliable;// for testing

    // Your data here
    boolean DEBUG = true;

    int seq = -1;
    Object value = null;
    public class PaxosInstanceArray{
        int prepareMax = 0;
        int acceptMax = -1;
        Object acceptValue = null;
        public Object decidedValue = null;
    }
    public HashMap<Integer, PaxosInstanceArray> seqInstanceMap;
    int[] doneMax;
    int seqMax = -1;
    Random rand;

    /**
     * Call the constructor to create a Paxos peer.
     * The hostnames of all the Paxos peers (including this one)
     * are in peers[]. The ports are in ports[].
     */
    public Paxos(int me, String[] peers, int[] ports){
        if(DEBUG){System.out.println("Paxos num: " + me + " In Paxos initialize");}
        this.me = me;
        this.peers = peers;
        this.ports = ports;
        this.mutex = new ReentrantLock();
        this.dead = new AtomicBoolean(false);
        this.unreliable = new AtomicBoolean(false);

        // Your initialization code here
        this.doneMax = new int[peers.length];
        rand = new Random();
        //rmeek adding this
        for(int i = 0; i < peers.length; i++){
            this.doneMax[i] = -1;
        }
        this.seqInstanceMap = new HashMap<Integer, paxos.Paxos.PaxosInstanceArray>();

        // register peers, do not modify this part
        try{
            System.setProperty("java.rmi.server.hostname", this.peers[this.me]);
            registry = LocateRegistry.createRegistry(this.ports[this.me]);
            stub = (PaxosRMI) UnicastRemoteObject.exportObject(this, this.ports[this.me]);
            registry.rebind("Paxos", stub);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Call() sends an RMI to the RMI handler on server with
     * arguments rmi name, request message, and server id. It
     * waits for the reply and return a response message if
     * the server responded, and return null if Call() was not
     * be able to contact the server.
     *
     * You should assume that Call() will time out and return
     * null after a while if it doesn't get a reply from the server.
     *
     * Please use Call() to send all RMIs and please don't change
     * this function.
     */
    public Response Call(String rmi, Request req, int id){
        Response callReply = null;

        PaxosRMI stub;
        try{
            Registry registry=LocateRegistry.getRegistry(this.ports[id]);
            stub=(PaxosRMI) registry.lookup("Paxos");
            if(rmi.equals("Prepare"))
                callReply = stub.Prepare(req);
            else if(rmi.equals("Accept"))
                callReply = stub.Accept(req);
            else if(rmi.equals("Decide"))
                callReply = stub.Decide(req);
            else
                System.out.println("Wrong parameters!");
        } catch(Exception e){
            return null;
        }
        return callReply;
    }


    /**
     * The application wants Paxos to start agreement on instance seq,
     * with proposed value v. Start() should start a new thread to run
     * Paxos on instance seq. Multiple instances can be run concurrently.
     *
     * Hint: You may start a thread using the runnable interface of
     * Paxos object. One Paxos object may have multiple instances, each
     * instance corresponds to one proposed value/command. Java does not
     * support passing arguments to a thread, so you may reset seq and v
     * in Paxos object before starting a new thread. There is one issue
     * that variable may change before the new thread actually reads it.
     * Test won't fail in this case.
     *
     * Start() just starts a new thread to initialize the agreement.
     * The application will call Status() to find out if/when agreement
     * is reached.
     */
    public void Start(int seq, Object value){
        if(DEBUG){System.out.println("Paxos num: " + me + " In Start this is seq number: " + seq + " value " + value);}

        int currentMin = this.Min();
        if(seq < currentMin){
            return;
        }
        if(seq > this.seqMax){
            this.seqMax = seq;
        }

        if(!this.seqInstanceMap.containsKey(seq)){
            this.seqInstanceMap.put(seq, new PaxosInstanceArray());
        }
        // @TODO: Why is return only if consensus is not arrived
        if(this.seqInstanceMap.get(seq).decidedValue != null){
            return;
        }
        while(this.seq != -1){} //wait for my previous start call to reset the seq var
        this.seq = seq;
        this.value = value;
        if(DEBUG){System.out.println("In Start and making a new Thread");}
        (new Thread(this)).start();
    }

    @Override
    public void run(){
        int mySeq = seq;
        seq = -1;
        long sleepTime = 10;
        if(DEBUG){System.out.println("Paxos num: " + me + " In Paxos.run");}
        while (true){

            // Housekeeping
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(sleepTime < 10000) {
                sleepTime = sleepTime*2;
            }
            if(this.isDead()) {
                return;
            }

            // Prepare phase
            Response retVal = new Response();
            int maxSeen = this.seqInstanceMap.get(mySeq).prepareMax;
            int peerLength = this.peers.length;
            int proposalID = ((maxSeen/peerLength) + 1)*(peerLength) + this.me; //pretty sure this gives unique ids
            if(this.PreparePhase(mySeq, value, retVal, proposalID)){
                if(DEBUG){System.out.println("Paxos: " + me + " Value chosen: this is retVal.proposalID: " + retVal.proposalID +
                        " retVal.value " + retVal.value);}
                // Accept Phase

                if(this.AcceptPhase(mySeq, retVal.value, retVal.proposalID)){
                    if(DEBUG){System.out.println("Paxos: " + me + " Value Accepted: this is retVal.proposalID: " + retVal.proposalID +
                            " retVal.value " + retVal.value);}
                    this.DecidePhase(mySeq, retVal.proposalID, retVal.value);
                    break;
                }

            }
        }

    }


    private boolean PreparePhase(int seq, Object value, Response retVal, int proposalID){
        if(DEBUG){System.out.println("Paxos num: " + me + " In Paxos.PreparePhase " +
           "seq: " + seq + " value: " + value + " ProposalID " + proposalID); }

        Response reply = null;
        Response highestAccept = new Response(0,-1,null);;
        int peerLength = this.peers.length;
        Request req = new Request(seq, proposalID);
        int respReceived = 0;
        for(int i = 0; i < peerLength; i++) {
            if (i == this.me) {
                reply = this.Prepare(req);
            } else {
                reply = Call("Prepare", req, i);
            }
            if (reply != null) {
                //System.out.println(("This is reply.Reply: " + reply.Reply + " from Machine: " + i));
                if (reply.Reply == 0) {
                    continue;
                }
                else if(reply.Reply == 1){
                    respReceived++;
                    if(reply.value != null && reply.proposalID > highestAccept.proposalID){
                        highestAccept.value = reply.value;
                        highestAccept.proposalID = reply.proposalID;
                    }
                }
            }
        }
        if(respReceived >= ((peerLength/2) + 1)){
            if(highestAccept.value != null){
                retVal.proposalID = proposalID;
                retVal.value = highestAccept.value;
            }
            else{
                retVal.proposalID = proposalID;
                retVal.value = value;
            }
            //System.out.println("Leaving Prepare this is retVal.proposalID: " + retVal.proposalID +
                    //" retVal.value " + retVal.value);
            return true;
        }

        if(DEBUG){System.out.println("Leaving Prepare no Value chosen");}
        return false;
    }

    // RMI handler
    public Response Prepare(Request req){
        // your code here
        //if(DEBUG){System.out.println("Paxos num: " + me + " In Paxos.Prepare and this is req " + req );}
        if(req.seq > seqMax){ seqMax = req.seq; };
        Response reply = new Response();
        this.mutex.lock();
        //if(DEBUG){System.out.println("Paxos num: " + me + " Past the lock " );}
        PaxosInstanceArray instance = this.seqInstanceMap.get(req.seq);
        //if(DEBUG){System.out.println("Paxos num: " + me + " In Paxos.Prepare and this is req " + req

        if(instance == null){
            instance = new PaxosInstanceArray();
            this.seqInstanceMap.put(req.seq, instance);
        }
        if(DEBUG){System.out.println("Paxos num: " + me + " In Paxos.Prepare and this is req.proposalID " +
                req.proposalID + " instace.prepareMax " + instance.prepareMax);}
        if(req.proposalID > instance.prepareMax){
            instance.prepareMax = req.proposalID;
            reply.value = instance.acceptValue;
            reply.proposalID = instance.acceptMax;
            reply.Reply = 1;
        }
        else {
            reply.Reply = 0;
        }
        //if(DEBUG){System.out.println("Paxos num: " + me + " reply.Reply: " + reply.Reply );}
        this.mutex.unlock();
        //if(DEBUG){System.out.println("Paxos num: " + me + " Leaving Paxos.Prepare and this is reply: " + reply);}
        return reply;
    }

    private boolean AcceptPhase(int seq, Object value, int proposalID){
        if(DEBUG){System.out.println("Paxos num " + me + " In Paxos.AcceptPhase");}
        int peerLength = this.peers.length;
        // Need twice the size since there could be failures in RMI
        Response reply = null;
        int respReceived = 0;

        for(int i = 0; i < peerLength; i++){
            Request req = new Request(seq, proposalID, value);
            if(i == this.me){
                reply = this.Accept(req);
            }
            else {
                reply = Call("Accept", req, i);
            }

            if(reply != null){
                if(DEBUG){System.out.println(("Accept: This is reply.Reply: " + reply.Reply + " from Machine: " + i));}
                if(reply.Reply == 0){
                    continue;
                }
                else if(reply.Reply == 1){
                    respReceived++;
                }
            }
        }
        if(respReceived >= ((peerLength/2) + 1)){
            if(DEBUG){System.out.println("Value accepted! proposalID: " + proposalID + " value: " + value);}
            return true;
        }

        return false;
    }

    public Response Accept(Request req){
        // your code here
        if(req.seq > seqMax){ seqMax = req.seq; };
        if(DEBUG){System.out.println("Paxos num: " + me + " In Paxos.Accept");}
        Response reply = new Response();
        this.mutex.lock();
        if(this.seqInstanceMap.get(req.seq) == null){
            this.seqInstanceMap.put(req.seq,new PaxosInstanceArray());
        }
        PaxosInstanceArray instance = this.seqInstanceMap.get(req.seq);
        if(req.proposalID >= instance.prepareMax){
            instance.acceptValue = req.value;
            instance.acceptMax = req.proposalID;
            instance.prepareMax = req.proposalID;
            reply.Reply = 1;
            reply.value = instance.acceptValue;
        }
        else {
            reply.Reply = 0;
        }
        this.mutex.unlock();
        return reply;
    }

    private void DecidePhase(int seq, int proposalID, Object value) {
        //if(DEBUG){System.out.println("Paxos num + " + me + " In Paxos.DecidePhase");}
        int peerLength = this.peers.length;
        Response reply = null;
        Request req = new Request(seq, proposalID, value,this.me,this.doneMax[this.me]);
        for(int i = 0; i < peerLength; i++){
            if(i == this.me){
                reply = this.Decide(req);
            }
            else{
                reply = Call("Decide", req, i);
            }
        }
    }

    public Response Decide(Request req){
        // your code here
        if(req.seq > seqMax){ seqMax = req.seq; };
        if(DEBUG){System.out.println("Paxos num " + me + " In Paxos.Decide");}
        this.mutex.lock();
        if(this.seqInstanceMap.get(req.seq) == null){
            this.seqInstanceMap.put(req.seq, new PaxosInstanceArray());
        }
        this.seqInstanceMap.get(req.seq).decidedValue = req.value;
        this.doneMax[req.me] = req.doneMaxMe;
        this.mutex.unlock();
        Response reply = new Response(1, -1, null);
        return  reply;
    }

    /**
     * The application on this machine is done with
     * all instances <= seq.
     *
     * see the comments for Min() for more explanation.
     */
    public void Done(int seq) {
        // Your code here
        if(DEBUG){System.out.println("Paxos num " + me + " In Paxos.Done");}
        this.mutex.lock();
        if(seq > this.doneMax[this.me]){
            this.doneMax[this.me] = seq;
        }
        int discard = this.Min();
        Iterator<Integer> i = seqInstanceMap.keySet().iterator();
        while(i.hasNext()){
            Integer thisSeq = i.next();
            if(thisSeq < discard){
                seqInstanceMap.remove(thisSeq);
            }
        }
        this.mutex.unlock();
    }


    /**
     * The application wants to know the
     * highest instance sequence known to
     * this peer.
     */
    public int Max(){
        // Your code here
        //if(DEBUG){System.out.println("Paxos num: " + me + " In Paxos.Max");}
        return seqMax;
    }

    /**
     * Min() should return one more than the minimum among z_i,
     * where z_i is the highest number ever passed
     * to Done() on peer i. A peers z_i is -1 if it has
     * never called Done().

     * Paxos is required to have forgotten all information
     * about any instances it knows that are < Min().
     * The point is to free up memory in long-running
     * Paxos-based servers.

     * Paxos peers need to exchange their highest Done()
     * arguments in order to implement Min(). These
     * exchanges can be piggybacked on ordinary Paxos
     * agreement protocol messages, so it is OK if one
     * peers Min does not reflect another Peers Done()
     * until after the next instance is agreed to.

     * The fact that Min() is defined as a minimum over
     * all Paxos peers means that Min() cannot increase until
     * all peers have been heard from. So if a peer is dead
     * or unreachable, other peers Min()s will not increase
     * even if all reachable peers call Done. The reason for
     * this is that when the unreachable peer comes back to
     * life, it will need to catch up on instances that it
     * missed -- the other peers therefore cannot forget these
     * instances.
     */
    public int Min(){
        // Your code here
        //if(DEBUG){System.out.println("Paxos num: " + me + " In Paxos.Min");}
        mutex.lock();
        int min = this.doneMax[this.me];
        for(int i = 0; i < this.doneMax.length; i++) {
            if (this.doneMax[i] < min) {
                min = this.doneMax[i];
            }
        }
        mutex.unlock();
        //if(DEBUG){System.out.println("Paxos num: " + me + " In Paxos.Min this is return val: " + (min + 1) );}
        return min + 1;
    }



    /**
     * the application wants to know whether this
     * peer thinks an instance has been decided,
     * and if so what the agreed value is. Status()
     * should just inspect the local peer state;
     * it should not contact other Paxos peers.
     */
    public retStatus Status(int seq){
        // Your code here
        //if(DEBUG){System.out.println("Paxos num: " + me + " In Paxos.Status");}
        int currentMin = this.Min();
        this.mutex.lock();
        if(seq < currentMin){
            this.mutex.unlock();
            return new retStatus(State.Forgotten, null);
        }
        //rmeek: changing this
        //Object decidedVal = this.seqInstanceMap.get(seq).decidedValue;
        PaxosInstanceArray instArr = this.seqInstanceMap.get(seq);
        if(instArr != null && instArr.decidedValue != null){
            this.mutex.unlock();
            return new retStatus(State.Decided, instArr.decidedValue);
        }
        this.mutex.unlock();
        return new retStatus(State.Pending, null);
    }

    /**
     * helper class for Status() return
     */
    public class retStatus{
        public State state;
        public Object v;

        public retStatus(State state, Object v){
            this.state = state;
            this.v = v;
        }
    }

    /**
     * Tell the peer to shut itself down.
     * For testing.
     * Please don't change these four functions.
     */
    public void Kill(){
        this.dead.getAndSet(true);
        if(this.registry != null){
            try {
                UnicastRemoteObject.unexportObject(this.registry, true);
            } catch(Exception e){
                System.out.println("None reference");
            }
        }
    }

    public boolean isDead(){
        return this.dead.get();
    }

    public void setUnreliable(){
        this.unreliable.getAndSet(true);
    }

    public boolean isunreliable(){
        return this.unreliable.get();
    }


}
